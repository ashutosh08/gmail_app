class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])

    session[:user_id] = user.id
    session[:user_name] = user.name
    session[:user_email] = user.email
    session[:image] = user.image
    session[:birthday] = user.birthday
    session[:gender] = user.gender
    redirect_to root_path
  end


  def destroy
    session[:user_id] = nil
    redirect_to root_path
  end
end